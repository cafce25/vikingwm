use slog::{o, Drain};
use smithay::reexports::{calloop::EventLoop, wayland_server::Display};
use std::{cell::RefCell, rc::Rc};
use vikingwm::{configuration::configuration::get_config, state::Vikingwm, backend::winit::WinitData};

pub fn setup() -> Vikingwm<WinitData> {
    let log = if std::env::var("ANVIL_MUTEX_LOG").is_ok() {
        slog::Logger::root(
            std::sync::Mutex::new(slog_term::term_full().fuse()).fuse(),
            o!(),
        )
    } else {
        slog::Logger::root(
            slog_async::Async::default(slog_term::term_full().fuse()).fuse(),
            o!(),
        )
    };
    let config = get_config(Some(&log));

    let event_loop = EventLoop::try_new().unwrap();
    let display = Rc::new(RefCell::new(Display::new()));
    let data = WinitData {};
    Vikingwm::init(
        config,
        display.clone(),
        event_loop.handle(),
        data,
        log.clone(),
        true,
    )
}
