use smithay::utils::{Logical, Point, Size};
use vikingwm::workspace::{window_map::Window, workspace::{Workspace,Layout}};
mod common;

fn resolutions() -> Vec<(i32, i32)>{
    vec![(1920, 1080), (3840,2160)] // 
}

fn add_windows(ws:&mut Workspace){
    for _ in 0..3 {
        let mut window = Window::create((0, 0).into(), &mut None);
        ws.insert(&mut window);
    }
}

#[test]
fn focussing_moving_windows() {
    let mut wm = common::setup();
    let mut ws = wm.current_ws();
    add_windows(&mut ws);
    for res in resolutions() {
        let (width, height) = res;
        ws.arrange((width, height).into(), None);
        assert_eq!(ws.current_window, 0);

        ws.next_window();
        assert_eq!(ws.current_window, 1);

        let loc = ws.windows()[ws.current_window].get_location();
        assert_eq!(loc,  (width / 2, 0).into());

        ws.move_next_window();
        assert_eq!(ws.current_window, 2);

        ws.arrange((width, height).into(), None); // Needs to be called here, in actual version will be called in loop

        let loc = ws.windows()[ws.current_window].get_location();
        assert_eq!(loc,  (width / 2, height / 2).into());

        ws.move_next_window();
        assert_eq!(ws.current_window, 0);
        ws.arrange((width, height).into(), None); // Needs to be called here, in actual version will be called in loop
        let loc = ws.windows()[ws.current_window].get_location();
        assert_eq!(loc,  (0,0).into());
    }
}

#[test]
fn tiling() {
    let mut wm = common::setup();
    let mut ws = wm.current_ws();
    add_windows(&mut ws);
    for res in resolutions() {
        let (width, height) = res;
        ws.arrange((width, height).into(), None);

        let expected: Vec<(Point<i32, Logical>, Size<i32, Logical>)> = vec![
            ((0, 0).into(), (width / 2, height).into()),
            ((width / 2, 0).into(), (width / 2, height / 2).into()),
            ((width / 2, height / 2).into(), (width / 2, height / 2).into()),
        ];
        for it in ws.windows().iter().zip(expected.iter()) {
            let (w, el) = it;
            assert_eq!(w.get_location(), el.0);
            assert_eq!(w.geometry().size, el.1);
        }
    }
}

#[test]
fn vert_tiling() {
    let mut wm = common::setup();
    let mut ws = wm.current_ws();
    add_windows(&mut ws);
    ws.layout(Layout::VertTiling);
    for res in resolutions() {
        let (width, height) = res;
        ws.arrange((width, height).into(), None);

        let expected: Vec<(Point<i32, Logical>, Size<i32, Logical>)> = vec![
            ((0, 0).into(), (width, height / 2).into()),
            ((0, height / 2).into(), (width / 2, height / 2).into()),
            ((width / 2, height / 2).into(), (width / 2, height / 2).into()),
        ];

        for it in ws.windows().iter().zip(expected.iter()) {
            let (w, el) = it;
            assert_eq!(w.get_location(), el.0);
            assert_eq!(w.geometry().size, el.1);
        }
    }
}

#[test]
fn fullscreen() {
    let mut wm = common::setup();
    let mut ws = wm.current_ws();
    add_windows(&mut ws);
    ws.layout(Layout::Fullscreen);
    for res in resolutions() {
        let (width, height) = res;
        ws.arrange((width, height).into(), None);

        // TODO: check if is in correct layer
        let expected: Vec<(Point<i32, Logical>, Size<i32, Logical>)> = vec![
            ((0, 0).into(), (width, height).into()),
            ((0, 0).into(), (0, 0).into()),
            ((0, 0).into(), (0, 0).into()),
        ];

        for it in ws.windows().iter().zip(expected.iter()) {
            let (w, el) = it;
            assert_eq!(w.get_location(), el.0);
            assert_eq!(w.geometry().size, el.1);
        }
    }
}

#[test]
fn floating() {
}
