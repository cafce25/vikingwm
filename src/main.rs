use slog::{Drain, Logger, o};
use vikingwm::configuration::configuration::get_config;
use std::fs::OpenOptions;
use anyhow::Result;


fn run(log: &Logger) -> Result<()> {
    let config = get_config(Some(log));
    #[cfg(feature = "debug")]
    vikingwm::backend::winit::run_winit(config, log);
    #[cfg(not(feature = "debug"))]
    vikingwm::backend::udev::run_udev(log);
    Ok(())
}


// I don't know about this...
struct Test {
    term: slog::Logger,
    file: slog::Logger,
}

impl Test {
    pub fn init(log_path: &str) -> Test {
        let file_opt = OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(log_path)
            .unwrap();

        let decorator = slog_term::PlainDecorator::new(file_opt);
        let drain = slog_term::FullFormat::new(decorator).build().fuse();
        let drain = slog_async::Async::new(drain).build().fuse();

        let file = slog::Logger::root(drain, o!());

        let decorator = slog_term::TermDecorator::new().build();
        let drain = slog_term::FullFormat::new(decorator).build().fuse();
        let drain = slog_async::Async::new(drain).build().fuse();

        let term = slog::Logger::root(drain, o!());

        slog_stdlog::init().expect("Could not setup log backend");
        Test { term , file }
    }

    pub fn log(&self, msg: &str) {
        slog::info!(self.file, "{}", msg);
        slog::info!(self.term, "{}", msg);
    }
}

fn main() {
    // let t = Test::init("log.log");
    // t.log("hello");
    // A logger facility, here we use the terminal
    let log = if std::env::var("ANVIL_MUTEX_LOG").is_ok() {
        slog::Logger::root(
            std::sync::Mutex::new(slog_term::term_full().fuse()).fuse(),
            o!(),
        )
    } else {
        slog::Logger::root(
            slog_async::Async::default(slog_term::term_full().fuse()).fuse(),
            o!(),
        )
    };

    let _guard = slog_scope::set_global_logger(log.clone());
    slog_stdlog::init().expect("Could not setup log backend");

    run(&log);
}
