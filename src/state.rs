use std::{
    cell::RefCell,
    rc::Rc,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Mutex,
    },
};
use anyhow::Result;

use smithay::{
    reexports::{
        calloop::{generic::Generic, Interest, LoopHandle, Mode, PostAction},
        wayland_protocols::unstable::xdg_decoration,
        // wayland_protocols::unstable::xdg_shell::server::xdg_toplevel::State,
        wayland_server::{protocol::wl_surface::WlSurface, Display},
    },
    utils::{Logical, Point},
    wayland::{
        data_device::{default_action_chooser, init_data_device, set_data_device_focus, DataDeviceEvent},
        output::xdg::init_xdg_output_manager,
        seat::{CursorImageStatus, KeyboardHandle, PointerHandle, Seat, XkbConfig},
        shell::xdg::decoration::{init_xdg_decoration_manager, XdgDecorationRequest},
        shm::init_shm_global,
        xdg_activation::{init_xdg_activation_global, XdgActivationEvent},
    },
};

#[cfg(feature = "xwayland")]
use smithay::xwayland::{XWayland, XWaylandEvent};

use crate::{output::output_map::OutputMap, keybind::Keybind, backend::{shell::ShellHandles,shell::init_shell,}, configuration::configuration::Config,  workspace::workspace::Workspace};

#[derive(Debug)]
pub struct Vikingwm<BackendData> {
    pub config: Config,
    pub backend_data: BackendData,
    pub socket_name: Option<String>,
    pub running: Arc<AtomicBool>,
    pub display: Rc<RefCell<Display>>,
    pub handle: LoopHandle<'static, Vikingwm<BackendData>>,
    //pub window_map: Rc<RefCell<crate::window_map::WindowMap>>,
    //pub workspaces: Vec<Rc<RefCell<Workspace>>>,
    pub workspaces: Vec<Workspace>,
    pub current_ws: usize,
    pub output_map: Rc<RefCell<crate::output::output_map::OutputMap>>,
    pub dnd_icon: Arc<Mutex<Option<WlSurface>>>,
    pub log: slog::Logger,
    // input-related fields
    pub pointer: PointerHandle,
    pub keyboard: KeyboardHandle,
    pub suppressed_keys: Vec<u32>,
    pub pointer_location: Point<f64, Logical>,
    pub cursor_status: Arc<Mutex<CursorImageStatus>>,
    pub seat_name: String,
    pub seat: Seat,
    pub start_time: std::time::Instant,
    pub shell_states: ShellHandles,
    pub keybinds: Vec<Keybind>,
    // things we must keep alive
    #[cfg(feature = "xwayland")]
    pub xwayland: XWayland<Vikingwm<BackendData>>,
}

impl<BackendData: Backend + 'static + std::fmt::Debug> Vikingwm<BackendData> {
    pub fn init(
        config: Config,
        display: Rc<RefCell<Display>>,
        handle: LoopHandle<'static, Vikingwm<BackendData>>,
        backend_data: BackendData,
        log: slog::Logger,
        listen_on_socket: bool,
    ) -> Vikingwm<BackendData> {
        // init the wayland connection
        handle
            .insert_source(
                Generic::from_fd(display.borrow().get_poll_fd(), Interest::READ, Mode::Level),
                move |_, _, state: &mut Vikingwm<BackendData>| {
                    let display = state.display.clone();
                    let mut display = display.borrow_mut();
                    match display.dispatch(std::time::Duration::from_millis(0), state) {
                        Ok(_) => Ok(PostAction::Continue),
                        Err(e) => {
                            error!(state.log, "I/O error on the Wayland display: {}", e);
                            state.running.store(false, Ordering::SeqCst);
                            Err(e)
                        }
                    }
                },
            )
            .expect("Failed to init the wayland event source.");

        // Init a window map, to track the location of our windows
        let mut workspaces = Vec::new();
        for ws in &config.screen.workspaces {
            workspaces.push(Workspace::new(ws.to_owned()));
        }

        let output_map = Rc::new(RefCell::new(OutputMap::new(
            display.clone(),
            workspaces.clone(),
            log.clone(),
        )));

        // Init the basic compositor globals
        init_shm_global(&mut (*display).borrow_mut(), vec![], log.clone());

        // Init the shell states
        let shell_states = init_shell::<BackendData>(display.clone(), log.clone());

        init_xdg_output_manager(&mut display.borrow_mut(), log.clone());
        init_xdg_activation_global(
            &mut display.borrow_mut(),
            |state, req, mut ddata| {
                let anvil_state = ddata.get::<Vikingwm<BackendData>>().unwrap();
                match req {
                    XdgActivationEvent::RequestActivation {
                        token,
                        token_data,
                        surface,
                    } => {
                        if token_data.timestamp.elapsed().as_secs() < 10 {
                            // Just grant the wish
                            // TODO: Fix looking up which workspaces we are on
                            anvil_state.workspaces[0].bring_surface_to_top(&surface);
                        } else {
                            // Discard the request
                            state.lock().unwrap().remove_request(&token);
                        }
                    }
                    XdgActivationEvent::DestroyActivationRequest { .. } => {}
                }
            },
            log.clone(),
        );

        init_xdg_decoration_manager(
            &mut display.borrow_mut(),
            |req, _ddata| match req {
                XdgDecorationRequest::NewToplevelDecoration { toplevel } => {
                    use xdg_decoration::v1::server::zxdg_toplevel_decoration_v1::Mode;

                    let res = toplevel.with_pending_state(|state| {
                        state.decoration_mode = Some(Mode::ServerSide);
                    });

                    if res.is_ok() {
                        toplevel.send_configure();
                    }
                }
                XdgDecorationRequest::SetMode { .. } => {}
                XdgDecorationRequest::UnsetMode { .. } => {}
            },
            log.clone(),
        );

        let socket_name = if listen_on_socket {
            let socket_name = display
                .borrow_mut()
                .add_socket_auto()
                .unwrap()
                .into_string()
                .unwrap();
            info!(log, "Listening on wayland socket"; "name" => socket_name.clone());
            ::std::env::set_var("WAYLAND_DISPLAY", &socket_name);
            Some(socket_name)
        } else {
            None
        };

        // init data device

        let dnd_icon = Arc::new(Mutex::new(None));

        let dnd_icon2 = dnd_icon.clone();
        init_data_device(
            &mut display.borrow_mut(),
            move |event| match event {
                DataDeviceEvent::DnDStarted { icon, .. } => {
                    *dnd_icon2.lock().unwrap() = icon;
                }
                DataDeviceEvent::DnDDropped => {
                    *dnd_icon2.lock().unwrap() = None;
                }
                _ => {}
            },
            default_action_chooser,
            log.clone(),
        );

        // init input
        let seat_name = backend_data.seat_name();

        let (mut seat, _) = Seat::new(&mut display.borrow_mut(), seat_name.clone(), log.clone());

        let cursor_status = Arc::new(Mutex::new(CursorImageStatus::Default));

        let cursor_status2 = cursor_status.clone();
        let pointer = seat.add_pointer(move |new_status| {
            // TODO: hide winit system cursor when relevant
            *cursor_status2.lock().unwrap() = new_status
        });

        let keyboard = seat
            .add_keyboard(XkbConfig::default(), 200, 25, |seat, focus| {
                set_data_device_focus(seat, focus.and_then(|s| s.as_ref().client()))
            })
            .expect("Failed to initialize the keyboard");

        #[cfg(feature = "xwayland")]
        let xwayland = {
            let (xwayland, channel) = XWayland::new(handle.clone(), display.clone(), log.clone());
            let ret = handle.insert_source(channel, |event, _, anvil_state| match event {
                XWaylandEvent::Ready { connection, client } => anvil_state.xwayland_ready(connection, client),
                XWaylandEvent::Exited => anvil_state.xwayland_exited(),
            });
            if let Err(e) = ret {
                error!(
                    log,
                    "Failed to insert the XWaylandSource into the event loop: {}", e
                );
            }
            xwayland
        };
        let current_ws = 0;
        let keybinds = config.keybinds.get();

        Vikingwm {
            config,
            backend_data,
            running: Arc::new(AtomicBool::new(true)),
            display,
            handle,
            workspaces,
            current_ws,
            output_map,
            dnd_icon,
            log,
            socket_name,
            pointer,
            keyboard,
            suppressed_keys: Vec::new(),
            cursor_status,
            pointer_location: (0.0, 0.0).into(),
            seat_name,
            shell_states,
            seat,
            start_time: std::time::Instant::now(),
            keybinds,
            #[cfg(feature = "xwayland")]
            xwayland,
        }
    }

    pub fn current_ws(&mut self) -> &mut Workspace {
        &mut self.workspaces[self.current_ws]
    }

    pub fn run(cfg : Config, log: &slog::Logger) -> Result<()>{
        Ok(())
    }
}

pub trait Backend {
    fn seat_name(&self) -> String;
}
