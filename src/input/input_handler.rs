use crate::state::Vikingwm;

#[cfg(feature = "udev")]
use crate::udev::UdevData;
#[cfg(feature = "winit")]
use crate::backend::winit::WinitData;

use smithay::backend::input::{InputBackend, InputEvent} ;

// #[cfg(any(feature = "winit", feature = "x11"))]
// use smithay::backend::input::PointerMotionAbsoluteEvent;

#[cfg(feature = "udev")]
use smithay::{
    backend::{
        input::{
            KeyboardKeyEvent, Device, DeviceCapability, PointerMotionEvent, ProximityState, TabletToolButtonEvent,
            TabletToolEvent, TabletToolProximityEvent, TabletToolTipEvent, TabletToolTipState,
        },
        session::Session,
    },
    utils::{Logical, Point},
    wayland::tablet_manager::{TabletDescriptor, TabletSeatTrait},
};
// use crate::input::{keyboard,pointer};

// TODO: implement action not implented
#[cfg(feature = "winit")]
impl Vikingwm<WinitData> {
    pub fn process_input_event<B: InputBackend>(&mut self, evt: InputEvent<B>) {
        match evt {
            InputEvent::Keyboard { event, .. } => {
                self.handle_keyboard::<B>(event);
            },
            InputEvent::PointerMotionAbsolute { event, .. } => {
                self.on_pointer_move_absolute::<B>(event)
            }
            InputEvent::PointerButton { event, .. } => self.on_pointer_button::<B>(event),
            InputEvent::PointerAxis { event, .. } => self.on_pointer_axis::<B>(event),
            _ => {
                // other events are not handled in vikingwm (yet)
            }
        }
    }
}

#[cfg(feature = "udev")]
impl Vikingwm<UdevData> {
    pub fn process_input_event<B: InputBackend>(&mut self, event: InputEvent<B>) {
        match event {
            InputEvent::Keyboard { event, .. } => match self.keyboard_key_to_action::<B>(event) {
                KeyAction::None => {}
                KeyAction::Quit => {
                    info!(self.log, "Quitting.");
                    self.running.store(false, Ordering::SeqCst);
                }
                #[cfg(feature = "udev")]
                KeyAction::VtSwitch(vt) => {
                    info!(self.log, "Trying to switch to vt {}", vt);
                    if let Err(err) = self.backend_data.session.change_vt(vt) {
                        error!(self.log, "Error switching to vt {}: {}", vt, err);
                    }
                }
                KeyAction::Run(cmd) => {
                    info!(self.log, "Starting program"; "cmd" => cmd.clone());
                    if let Err(e) = Command::new(&cmd).spawn() {
                        error!(self.log,
                            "Failed to start program";
                            "cmd" => cmd,
                            "err" => format!("{:?}", e)
                        );
                    }
                }
                KeyAction::Screen(num) => {
                    let geometry = self.output_map.borrow().find_by_index(num).map(|o| o.geometry());

                    if let Some(geometry) = geometry {
                        let x = geometry.loc.x as f64 + geometry.size.w as f64 / 2.0;
                        let y = geometry.size.h as f64 / 2.0;
                        self.pointer_location = (x, y).into()
                    }
                }
                KeyAction::ScaleUp => {
                    let mut output_map = self.output_map.borrow_mut();

                    let output = output_map
                        .find_by_position(self.pointer_location.to_i32_round())
                        .map(|o| (o.name().to_owned(), o.location(), o.scale()));

                    if let Some((name, output_location, scale)) = output {
                        let new_scale = scale + 0.25;

                        output_map.update_scale_by_name(new_scale, name);

                        let rescale = scale as f64 / new_scale as f64;
                        let output_location = output_location.to_f64();
                        let mut pointer_output_location = self.pointer_location - output_location;
                        pointer_output_location.x *= rescale;
                        pointer_output_location.y *= rescale;
                        self.pointer_location = output_location + pointer_output_location;

                        let under = self.workspaces[0].borrow().get_surface_under(self.pointer_location);// TODO: fix look up which workspace we're on
                        self.pointer
                            .motion(self.pointer_location, under, SCOUNTER.next_serial(), 0);
                    }
                }
                KeyAction::ScaleDown => {
                    let mut output_map = self.output_map.borrow_mut();

                    let output = output_map
                        .find_by_position(self.pointer_location.to_i32_round())
                        .map(|o| (o.name().to_owned(), o.location(), o.scale()));

                    if let Some((name, output_location, scale)) = output {
                        let new_scale = f32::max(1.0, scale - 0.25);
                        output_map.update_scale_by_name(new_scale, name);

                        let rescale = scale as f64 / new_scale as f64;
                        let output_location = output_location.to_f64();
                        let mut pointer_output_location = self.pointer_location - output_location;
                        pointer_output_location.x *= rescale;
                        pointer_output_location.y *= rescale;
                        self.pointer_location = output_location + pointer_output_location;

                        let under = self.workspaces[0].borrow().get_surface_under(self.pointer_location);// TODO: fix look up which workspace we're on
                        self.pointer
                            .motion(self.pointer_location, under, SCOUNTER.next_serial(), 0);
                    }
                }
            },
            InputEvent::PointerMotion { event, .. } => self.on_pointer_move::<B>(event),
            InputEvent::PointerButton { event, .. } => self.on_pointer_button::<B>(event),
            InputEvent::PointerAxis { event, .. } => self.on_pointer_axis::<B>(event),
            InputEvent::TabletToolAxis { event, .. } => self.on_tablet_tool_axis::<B>(event),
            InputEvent::TabletToolProximity { event, .. } => self.on_tablet_tool_proximity::<B>(event),
            InputEvent::TabletToolTip { event, .. } => self.on_tablet_tool_tip::<B>(event),
            InputEvent::TabletToolButton { event, .. } => self.on_tablet_button::<B>(event),
            InputEvent::DeviceAdded { device } => {
                if device.has_capability(DeviceCapability::TabletTool) {
                    self.seat
                        .tablet_seat()
                        .add_tablet(&TabletDescriptor::from(&device));
                }
            }
            InputEvent::DeviceRemoved { device } => {
                if device.has_capability(DeviceCapability::TabletTool) {
                    let tablet_seat = self.seat.tablet_seat();

                    tablet_seat.remove_tablet(&TabletDescriptor::from(&device));

                    // If there are no tablets in seat we can remove all tools
                    if tablet_seat.count_tablets() == 0 {
                        tablet_seat.clear_tools();
                    }
                }
            }
            _ => {
                // other events are not handled in anvil (yet)
            }
        }
    }

    fn on_pointer_move<B: InputBackend>(&mut self, evt: B::PointerMotionEvent) {
        let serial = SCOUNTER.next_serial();
        self.pointer_location += evt.delta();

        // clamp to screen limits
        // this event is never generated by winit
        self.pointer_location = self.clamp_coords(self.pointer_location);

        let under = self.workspaces[0].borrow().get_surface_under(self.pointer_location);// TODO: fix look up which workspace we're on
        self.pointer
            .motion(self.pointer_location, under, serial, evt.time());
    }

    fn on_tablet_tool_axis<B: InputBackend>(&mut self, evt: B::TabletToolAxisEvent) {
        let output_map = self.output_map.borrow();
        let pointer_location = &mut self.pointer_location;
        let tablet_seat = self.seat.tablet_seat();
        let window_map = self.workspaces[0].borrow();// TODO: fix look up which workspace we're on

        let output_geometry = output_map.with_primary().map(|o| o.geometry());

        if let Some(rect) = output_geometry {
            *pointer_location = evt.position_transformed(rect.size) + rect.loc.to_f64();

            let under = window_map.get_surface_under(*pointer_location);
            let tablet = tablet_seat.get_tablet(&TabletDescriptor::from(&evt.device()));
            let tool = tablet_seat.get_tool(&evt.tool());

            if let (Some(tablet), Some(tool)) = (tablet, tool) {
                if evt.pressure_has_changed() {
                    tool.pressure(evt.pressure());
                }
                if evt.distance_has_changed() {
                    tool.distance(evt.distance());
                }
                if evt.tilt_has_changed() {
                    tool.tilt(evt.tilt());
                }
                if evt.slider_has_changed() {
                    tool.slider_position(evt.slider_position());
                }
                if evt.rotation_has_changed() {
                    tool.rotation(evt.rotation());
                }
                if evt.wheel_has_changed() {
                    tool.wheel(evt.wheel_delta(), evt.wheel_delta_discrete());
                }

                tool.motion(
                    *pointer_location,
                    under,
                    &tablet,
                    SCOUNTER.next_serial(),
                    evt.time(),
                );
            }
        }
    }

    fn on_tablet_tool_proximity<B: InputBackend>(&mut self, evt: B::TabletToolProximityEvent) {
        let output_map = self.output_map.borrow();
        let pointer_location = &mut self.pointer_location;
        let tablet_seat = self.seat.tablet_seat();
        let window_map = self.workspaces[0].borrow();// TODO: fix look up which workspace we're on

        let output_geometry = output_map.with_primary().map(|o| o.geometry());

        if let Some(rect) = output_geometry {
            let tool = evt.tool();
            tablet_seat.add_tool(&tool);

            *pointer_location = evt.position_transformed(rect.size) + rect.loc.to_f64();

            let under = window_map.get_surface_under(*pointer_location);
            let tablet = tablet_seat.get_tablet(&TabletDescriptor::from(&evt.device()));
            let tool = tablet_seat.get_tool(&tool);

            if let (Some(under), Some(tablet), Some(tool)) = (under, tablet, tool) {
                match evt.state() {
                    ProximityState::In => tool.proximity_in(
                        *pointer_location,
                        under,
                        &tablet,
                        SCOUNTER.next_serial(),
                        evt.time(),
                    ),
                    ProximityState::Out => tool.proximity_out(evt.time()),
                }
            }
        }
    }

    fn on_tablet_tool_tip<B: InputBackend>(&mut self, evt: B::TabletToolTipEvent) {
        let tool = self.seat.tablet_seat().get_tool(&evt.tool());

        if let Some(tool) = tool {
            match evt.tip_state() {
                TabletToolTipState::Down => {
                    tool.tip_down(SCOUNTER.next_serial(), evt.time());

                    // change the keyboard focus unless the pointer is grabbed
                    if !self.pointer.is_grabbed() {
                        let under = self
                            .workspaces[0]// TODO: fix look up which workspace we're on
                            .borrow_mut()
                            .get_surface_and_bring_to_top(self.pointer_location);

                        let serial = SCOUNTER.next_serial();
                        self.keyboard
                            .set_focus(under.as_ref().map(|&(ref s, _)| s), serial);
                    }
                }
                TabletToolTipState::Up => {
                    tool.tip_up(evt.time());
                }
            }
        }
    }

    fn on_tablet_button<B: InputBackend>(&mut self, evt: B::TabletToolButtonEvent) {
        let tool = self.seat.tablet_seat().get_tool(&evt.tool());

        if let Some(tool) = tool {
            tool.button(
                evt.button(),
                evt.button_state(),
                SCOUNTER.next_serial(),
                evt.time(),
            );
        }
    }

    fn clamp_coords(&self, pos: Point<f64, Logical>) -> Point<f64, Logical> {
        if self.output_map.borrow().is_empty() {
            return pos;
        }

        let (pos_x, pos_y) = pos.into();
        let output_map = self.output_map.borrow();
        let max_x = output_map.width();
        let clamped_x = pos_x.max(0.0).min(max_x as f64);
        let max_y = output_map.height(clamped_x as i32);

        if let Some(max_y) = max_y {
            let clamped_y = pos_y.max(0.0).min(max_y as f64);

            (clamped_x, clamped_y).into()
        } else {
            (clamped_x, pos_y).into()
        }
    }
}

#[cfg(feature = "x11")]
impl Vikingwm<X11Data> {
    pub fn process_input_event<B: InputBackend>(&mut self, event: InputEvent<B>) {
        match event {
            InputEvent::Keyboard { event } => match self.keyboard_key_to_action::<B>(event) {
                KeyAction::None => (),

                KeyAction::Quit => {
                    info!(self.log, "Quitting.");
                    self.running.store(false, Ordering::SeqCst);
                }

                KeyAction::Run(cmd) => {
                    info!(self.log, "Starting program"; "cmd" => cmd.clone());

                    if let Err(e) = Command::new(&cmd).spawn() {
                        error!(self.log,
                            "Failed to start program";
                            "cmd" => cmd,
                            "err" => format!("{:?}", e)
                        );
                    }
                }

                KeyAction::ScaleUp => {
                    let current_scale = {
                        self.output_map
                            .borrow()
                            .find_by_name(crate::x11::OUTPUT_NAME)
                            .map(|o| o.scale())
                            .unwrap_or(1.0)
                    };

                    self.output_map
                        .borrow_mut()
                        .update_scale_by_name(current_scale + 0.25f32, crate::x11::OUTPUT_NAME);
                }

                KeyAction::ScaleDown => {
                    let current_scale = {
                        self.output_map
                            .borrow()
                            .find_by_name(crate::x11::OUTPUT_NAME)
                            .map(|o| o.scale())
                            .unwrap_or(1.0)
                    };

                    self.output_map.borrow_mut().update_scale_by_name(
                        f32::max(1.0f32, current_scale + 0.25f32),
                        crate::x11::OUTPUT_NAME,
                    );
                }

                action => {
                    warn!(self.log, "Key action {:?} unsupported on x11 backend.", action);
                }
            },

            InputEvent::PointerMotionAbsolute { event } => self.on_pointer_move_absolute::<B>(event),
            InputEvent::PointerButton { event } => self.on_pointer_button::<B>(event),
            InputEvent::PointerAxis { event } => self.on_pointer_axis::<B>(event),
            _ => (), // other events are not handled in anvil (yet)
        }
    }

    fn on_pointer_move_absolute<B: InputBackend>(&mut self, evt: B::PointerMotionAbsoluteEvent) {
        let output_size = self
            .output_map
            .borrow()
            .find_by_name(crate::x11::OUTPUT_NAME)
            .map(|o| o.size())
            .unwrap();

        let pos = evt.position_transformed(output_size);
        self.pointer_location = pos;
        let serial = SCOUNTER.next_serial();
        let under = self.window_map.borrow().get_surface_under(pos);
        self.pointer.motion(pos, under, serial, evt.time());
    }
}
