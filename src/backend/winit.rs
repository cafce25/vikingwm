use std::{cell::RefCell, rc::Rc, sync::atomic::Ordering, time::Duration};

// #[cfg(feature = "debug")]
// use smithay::backend::renderer::gles2::Gles2Texture;
#[cfg(feature = "egl")]
use smithay::{
    backend::renderer::{ImportDma, ImportEgl},
    wayland::dmabuf::init_dmabuf_global,
};
use smithay::{
    backend::{
        winit::{self, WinitEvent},
        SwapBuffersError,
    },
    reexports::{
        calloop::EventLoop,
        wayland_server::{protocol::wl_output, Display},
    },
    wayland::{
        output::{Mode, PhysicalProperties},
        seat::CursorImageStatus,
    },
};

use slog::Logger;

use crate::backend::{drawing::*, render::render_layers_and_windows};
use crate::{
    configuration::configuration::Config,
    state::{Backend, Vikingwm},
    // workspace::workspace::Workspace,
};

pub const OUTPUT_NAME: &str = "winit";

#[derive(Debug)]
pub struct WinitData;

impl Backend for WinitData {
    fn seat_name(&self) -> String {
        String::from("winit")
    }
}

pub fn run_winit(config: Config, log: &Logger) {
    let mut event_loop = EventLoop::try_new().unwrap();
    let display = Rc::new(RefCell::new(Display::new()));

    let (renderer, mut winit) = match winit::init(log.clone()) {
        Ok(ret) => ret,
        Err(err) => {
            slog::crit!(log, "Failed to initialize Winit backend: {}", err);
            return;
        }
    };
    let renderer = Rc::new(RefCell::new(renderer));

    #[cfg(feature = "egl")]
    if renderer
        .borrow_mut()
        .renderer()
        .bind_wl_display(&display.borrow())
        .is_ok()
    {
        info!(log, "EGL hardware-acceleration enabled");
        let dmabuf_formats = renderer
            .borrow_mut()
            .renderer()
            .dmabuf_formats()
            .cloned()
            .collect::<Vec<_>>();
        let renderer = renderer.clone();
        init_dmabuf_global(
            &mut *display.borrow_mut(),
            dmabuf_formats,
            move |buffer, _| {
                renderer
                    .borrow_mut()
                    .renderer()
                    .import_dmabuf(buffer)
                    .is_ok()
            },
            log.clone(),
        );
    };

    let size = renderer.borrow().window_size().physical_size;

    /*
     * Initialize the globals
     */

    let data = WinitData {};
    // init state, where state is like a holder of everything
    let mut state = Vikingwm::init(
        config,
        display.clone(),
        event_loop.handle(),
        data,
        log.clone(),
        true,
    );

    for output in &state.config.screen.outputs {
        let mode = Mode {
            size,
            refresh: output.refresh,
        };

        state.output_map.borrow_mut().add(
            &output.name,
            PhysicalProperties {
                size: (0, 0).into(),
                subpixel: wl_output::Subpixel::Unknown,
                make: "Smithay".into(),
                model: "Winit".into(),
            },
            mode,
        );
    }

    let start_time = std::time::Instant::now();
    let mut cursor_visible = true;

    //#[cfg(feature = "xwayland")]
    //state.start_xwayland();

    info!(log, "Initialization completed, starting the main loop.");

    while state.running.load(Ordering::SeqCst) {
        if winit
            .dispatch_new_events(|event| match event {
                WinitEvent::Resized { size, .. } => {
                    for conf_output in &state.config.screen.outputs {
                        state.output_map.borrow_mut().update_mode_by_name(
                            Mode {
                                size,
                                refresh: conf_output.refresh,
                            },
                            &conf_output.name,
                        );

                        let output_mut = state.output_map.borrow();
                        let output = output_mut.find_by_name(&conf_output.name).unwrap();

                        for ws in &mut state.workspaces {
                            ws.layers.arange_layers(output);
                        }
                    }
                }

                WinitEvent::Input(event) => state.process_input_event(event),

                _ => (),
            })
            .is_err()
        {
            state.running.store(false, Ordering::SeqCst);
            break;
        }

        // drawing logic
        {
            // for output in &state.config.screen.outputs{
            //     let output_name = &output.name;
            let output_name = &state.config.screen.outputs[0].name; // To show all 'outputs', we should probably organize them somewhere
                let mut renderer = renderer.borrow_mut();
                // This is safe to do as with winit we are guaranteed to have exactly one output
                let (output_geometry, output_scale) = state
                    .output_map
                    .borrow()
                    .find_by_name(output_name)
                    .map(|output| (output.geometry(), output.scale()))
                    .unwrap();
                    // dbg!(&output_geometry);
                    // dbg!(&output_scale);

                let mut ws = &mut state.workspaces[state.current_ws];

                let result = renderer
                    .render(|renderer, frame| {
                        render_layers_and_windows(
                            renderer,
                            frame,
                            &mut ws,
                            output_geometry,
                            output_scale,
                            &log,
                        )?;

                        let (x, y) = state.pointer_location.into();

                        // draw the dnd icon if any
                        {
                            let guard = state.dnd_icon.lock().unwrap();
                            if let Some(ref surface) = *guard {
                                if surface.as_ref().is_alive() {
                                    draw_dnd_icon(
                                        renderer,
                                        frame,
                                        surface,
                                        (x as i32, y as i32).into(),
                                        output_scale,
                                        &log,
                                    )?;
                                }
                            }
                        }
                        // draw the cursor as relevant
                        {
                            let mut guard = state.cursor_status.lock().unwrap();
                            // reset the cursor if the surface is no longer alive
                            let mut reset = false;
                            if let CursorImageStatus::Image(ref surface) = *guard {
                                reset = !surface.as_ref().is_alive();
                            }
                            if reset {
                                *guard = CursorImageStatus::Default;
                            }

                            // draw as relevant
                            if let CursorImageStatus::Image(ref surface) = *guard {
                                cursor_visible = false;
                                draw_cursor(
                                    renderer,
                                    frame,
                                    surface,
                                    (x as i32, y as i32).into(),
                                    output_scale,
                                    &log,
                                )?;
                            } else {
                                    cursor_visible = true;
                                }
                        }

                        Ok(())
                    })
                    .map_err(Into::<SwapBuffersError>::into)
                    .and_then(|x| x);

                renderer.window().set_cursor_visible(cursor_visible);

                if let Err(SwapBuffersError::ContextLost(err)) = result {
                    error!(log, "Critical Rendering Error: {}", err);
                    state.running.store(false, Ordering::SeqCst);
                }
            // }
        }

        // Send frame events so that client start drawing their next frame
        state
            .current_ws()
            .send_frames(start_time.elapsed().as_millis() as u32);
        display.borrow_mut().flush_clients(&mut state);

        if event_loop
            .dispatch(Some(Duration::from_millis(16)), &mut state) // Why 16 ms?
            .is_err()
        {
            state.running.store(false, Ordering::SeqCst);
        } else {
            display.borrow_mut().flush_clients(&mut state);
            state.current_ws().refresh();
            state.output_map.borrow_mut().refresh();
        }
    }

    // Cleanup stuff
    state.current_ws().clear(); // TODO: fix look up which workspace we're on
}
