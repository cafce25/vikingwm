use crate::{
    keybind::{Action, Direction, KeyAction, Keybind},
    workspace::workspace,
};
use anyhow::Result;
use serde::Deserialize;
use slog::Logger;
use smithay::wayland::seat::{Keysym, ModifiersState};
use std::{env, fs};

#[derive(Debug, Deserialize)]
pub struct Config {
    pub screen: Screen,
    pub keybinds: Keybinds,
}

// TODO: Think about what to do when there is only 1 output, what should be optional here? Or
// should the outputs vec be optional?
#[derive(Debug, Deserialize)]
pub struct Output {
    pub name: String,
    pub location: String,
    pub resolution: String,
    pub rotation: u8,
    pub refresh: i32,
    pub layout: String,
}

#[derive(Debug, Deserialize)]
pub struct Screen {
    pub outputs: Vec<Output>,
    pub workspaces: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct Keybinds {
    pub spawn: Vec<(String, String)>,
    pub window: Vec<(String, String)>,
    pub workspace: Vec<(String, String)>,
    pub wm: Vec<(String, String)>,
}

fn new_empty_modstate() -> ModifiersState {
    ModifiersState {
        logo: false,
        alt: false,
        caps_lock: false,
        ctrl: false,
        shift: false,
        num_lock: false,
    }
}
pub fn new_modstate(keys: Vec<&str>) -> ModifiersState {
    let mut mods = new_empty_modstate();

    for current in keys {
        dbg!(&::xkbcommon::xkb::keysym_get_name(
            ::xkbcommon::xkb::keysym_from_name(current, xkbcommon::xkb::KEYSYM_NO_FLAGS)
        ));
        let key = ::xkbcommon::xkb::keysym_from_name(current, xkbcommon::xkb::KEYSYM_NO_FLAGS);
        if key == xkbcommon::xkb::KEY_NoSymbol {
            match current {
                #[cfg(feature = "debug")]
                "super" => mods.alt = true,
                #[cfg(not(feature = "debug"))]
                "super" => mods.logo = true,
                "ctrl" => mods.ctrl = true,
                "shift" => mods.shift = true,
                "num_lock" => mods.num_lock = true,
                "caps_lock" => mods.caps_lock = true,
                _ => {} // xkbcommon::xkb::keysym_from_name(&m, xkbcommon::xkb::KEYSYM_NO_FLAGS)
            }
        }
    }
    mods
}

impl Keybinds {
    fn to_action(s: &str) -> KeyAction {
        use Action::*;
        use Direction::*;
        use KeyAction::*;

        match s {
            "next_window" => Window(Focus, Next, false),
            "prev_window" => Window(Focus, Prev, false),
            "move_next_window" => Window(Move, Next, false),
            "move_prev_window" => Window(Move, Prev, false),
            "close" => Close,
            "reload" => Reload,
            "quit" => Quit,
            "next_output" => Output(Next),
            "prev_output" => Output(Prev),
            "move_next_output" => Window(Move, Next, true),
            "move_prev_output" => Window(Move, Prev, true),
            "set_layout_tiling" => Layout(workspace::Layout::Tiling),
            "set_layout_vert_tiling" => Layout(workspace::Layout::VertTiling),
            "set_layout_float" =>Layout(workspace::Layout::Floating),
            "set_layout_fullscreen" =>Layout(workspace::Layout::Fullscreen),
            _ => {
                // any digit would be a Workspace
                if let Ok(val) = u8::from_str_radix(s, 10) {
                    return Workspace(val);
                }
                // anything else would be a Run
                else {
                    return Run(s.into());
                }
            }
        }
    }

    fn as_vec(&self) -> Vec<&(String, String)> {
        let mut keybinds = Vec::new();
        keybinds.extend(self.wm.iter());
        keybinds.extend(self.spawn.iter());
        keybinds.extend(self.window.iter());
        keybinds.extend(self.workspace.iter());
        keybinds
    }

    pub fn get(&self) -> Vec<Keybind> {
        let mut keybinds = Vec::new();
        for keybind in self.as_vec() {
            let (cmd, kb) = keybind;
            let splitted_kb: Vec<&str> = kb.split(" + ").collect();
            let kb = Keybind::new(splitted_kb, Some(Keybinds::to_action(cmd)));
            keybinds.push(kb);
        }
        keybinds
    }
}

// TODO: better error handling
fn parse_config(path: &str) -> Config {
    let config = fs::read_to_string(path).expect("Could not find the configuration file");
    toml::from_str(&config).expect("Could not parse configuation")
}

#[cfg(feature = "debug")]
pub fn get_config(logger: Option<&Logger>) -> Config {
    if let Some(log) = logger {
        slog::info!(log, "Opening the debug_config.toml file");
    }
    parse_config("debug_config.toml")
}

#[cfg(not(feature = "debug"))]
// get path: $XDG_CONFIG_HOME/vikingwm/config.toml
// TODO: copy default_config to path if not there available yet
pub fn get_config(logger: Option<&Logger>) -> Config {
    let xdg_set = env::var("XDG_CONFIG_HOME");
    let config_path;
    if xdg_set.is_ok() {
        config_path = format!("{}/vikingwm/config.toml", &xdg_set.unwrap());
    } else {
        config_path = "~/.config/vikingwm/config.toml".into();
    };

    if let Some(log) = logger {
        slog::info!(log, "Opening the {} file", config_path);
    }
    parse_config(&config_path)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn keybinds() {
        let cfg = get_config(None);
        let kb = cfg.keybinds.get();
        dbg!(&kb);
        let expected = [
            Keybind::new(vec!["super", "ctrl", "r"], Some(KeyAction::Reload)),
            Keybind::new(vec!["super", "ctrl", "q"], Some(KeyAction::Quit)),
            Keybind::new(vec!["super", "b"], Some(KeyAction::Run("$BROWSER".into()))),
            Keybind::new(
                vec!["super", "Return"],
                Some(KeyAction::Run("$TERMINAL".into())),
            ),
            Keybind::new(
                vec!["super", "j"],
                Some(KeyAction::Window(Action::Focus, Direction::Next, false)),
            ),
            Keybind::new(
                vec!["super", "k"],
                Some(KeyAction::Window(Action::Focus, Direction::Prev, false)),
            ),
            Keybind::new(vec!["super", "."], Some(KeyAction::Output(Direction::Next))),
            Keybind::new(vec!["super", ","], Some(KeyAction::Output(Direction::Prev))),
            Keybind::new(
                vec!["super", "shift", "J"],
                Some(KeyAction::Window(Action::Move, Direction::Next, false)),
            ),
            Keybind::new(
                vec!["super", "shift", "K"],
                Some(KeyAction::Window(Action::Move, Direction::Prev, false)),
            ),
            Keybind::new(
                vec!["super", "shift", ">"],
                Some(KeyAction::Window(Action::Move, Direction::Next, true)),
            ),
            Keybind::new(
                vec!["super", "shift", "<"],
                Some(KeyAction::Window(Action::Move, Direction::Prev, true)),
            ),
            Keybind::new(vec!["super", "q"], Some(KeyAction::Close)),
            Keybind::new(vec!["super", "t"], Some(KeyAction::Layout(workspace::Layout::Tiling))),
            Keybind::new(vec!["super", "shift", "T"], Some(KeyAction::Layout(workspace::Layout::VertTiling))),
            Keybind::new(vec!["super", "shift", "F"], Some(KeyAction::Layout(workspace::Layout::Floating))),
            Keybind::new(vec!["super", "f"], Some(KeyAction::Layout(workspace::Layout::Fullscreen))),

            Keybind::new(vec!["super", "1"], Some(KeyAction::Workspace(1))),
            Keybind::new(vec!["super", "2"], Some(KeyAction::Workspace(2))),
            Keybind::new(vec!["super", "3"], Some(KeyAction::Workspace(3))),
            Keybind::new(vec!["super", "4"], Some(KeyAction::Workspace(4))),
            Keybind::new(vec!["super", "5"], Some(KeyAction::Workspace(5))),
            Keybind::new(vec!["super", "6"], Some(KeyAction::Workspace(6))),
            Keybind::new(vec!["super", "7"], Some(KeyAction::Workspace(7))),
            Keybind::new(vec!["super", "8"], Some(KeyAction::Workspace(8))),
            Keybind::new(vec!["super", "9"], Some(KeyAction::Workspace(9))),
        ];
        for (a, b) in kb.into_iter().zip(expected) {
            assert_eq!(a, b);
        }
    }
    #[test]
    fn keybinds_to_vec() {
        let cfg = get_config(None);
        let vec_of_keybinds = cfg.keybinds.as_vec();

        let expected: Vec<(String, String)> = vec![
            ("reload".into(), "super + ctrl + r".into()),
            ("quit".into(), "super + ctrl + q".into()),
            ("$BROWSER".into(), "super + b".into()),
            ("$TERMINAL".into(), "super + Return".into()),
            ("next_window".into(), "super + j".into()),
            ("prev_window".into(), "super + k".into()),
            ("next_output".into(), "super + .".into()),
            ("prev_output".into(), "super + ,".into()),
            ("move_next_window".into(), "super + shift + J".into()),
            ("move_prev_window".into(), "super + shift + K".into()),
            ("move_next_output".into(), "super + shift + >".into()),
            ("move_prev_output".into(), "super + shift + <".into()),
            ("close".into(), "super + q".into()),
            ("set_layout_tiling".into(), "super + t".into()),
            ("set_layout_vert_tiling".into(), "super + shift + T".into()),
            ("set_layout_float".into(), "super + shift + F".into()),
            ("set_layout_fullscreen".into(), "super + f".into()),
            ("1".into(), "super + 1".into()),
            ("2".into(), "super + 2".into()),
            ("3".into(), "super + 3".into()),
            ("4".into(), "super + 4".into()),
            ("5".into(), "super + 5".into()),
            ("6".into(), "super + 6".into()),
            ("7".into(), "super + 7".into()),
            ("8".into(), "super + 8".into()),
            ("9".into(), "super + 9".into()),
        ];

        assert_eq!(vec_of_keybinds.len(), expected.len());
        for (a, b) in vec_of_keybinds.into_iter().zip(expected) {
            assert_eq!(a, &b);
        }
    }
}
